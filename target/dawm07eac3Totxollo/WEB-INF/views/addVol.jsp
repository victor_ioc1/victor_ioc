<%-- 
    Document   : addVol
    Created on : 8 may 2023, 20:36:08
    Author     : T440S
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <title>JSP Page</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-2">
                <img id="logo" style="width: 128px;" src="img/totxollo.png"/>
            </div>
            <div class="col-md-10">
                <nav>
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active">
                            <a href="<spring:url value= '/'/>">Inici</a>
                        </li>
                        <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
                            <li role="Presentation" class="">
                                <a href="<c:url value="/j_spring_security_logout" />" class="btn btndanger btn-mini pull-right">desconnectar</a>
                            </li>
                        </sec:authorize>
                    </ul>
                </nav>
            </div>
        </div>
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>${banner}</h1>
                    <p>${tagline}</p>
                </div>
            </div>
        </section>
        <section class="container">
            <form:form modelAttribute="newXollo" class="form-horizontal">
                <fieldset>
                    <legend>Afegir vol</legend>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="codiVol">Codi</label>
                        <div class="col-lg-10">
                            <form:input id="codiVol" path="codi" type="text" class="form:input-large"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="titolVol">Titol</label>
                        <div class="col-lg-10">
                            <form:input id="titolVol" path="titol" type="text" class="form:input-large"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2"
                               for="desc">Descripció</label>
                        <div class="col-lg-10">
                            <form:textarea id="desc" path="desc" rows ="2"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="unitatsVol">Unitats</label>
                        <div class="col-lg-10">
                            <form:input id="unitatsVol" path="numUnitats" type="number" class="form:input-large"/>
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="desti">Destí</label>
                        <div class="col-lg-10">
                            <form:input id="desti" path="desti" type="text" class="form:input-large"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="data">Data</label>
                        <div class="col-lg-10">
                            <form:input id="data" path="data" type="date" class="form:input-large"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="persones">Persones</label>
                        <div class="col-lg-10">
                            <form:input id="persones" path="persones" type="number" class="form:input-large"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <input type="submit" id="btnAdd" class="btn btn-primary"
                                   value ="Crear"/>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </section>
    </body>
</html>
