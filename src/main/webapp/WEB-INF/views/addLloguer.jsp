<%-- 
    Document   : addLloguer
    Created on : 9 may 2023, 0:53:53
    Author     : T440S
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <title>JSP Page</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-2">
                <img id="logo" style="width: 128px;" src="img/totxollo.png"/>
            </div>
            <div class="col-md-10">
                <nav>
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active">
                            <a href="<spring:url value= '/'/>">Inici</a>
                        </li>
                        <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
                            <li role="Presentation" class="">
                                <a href="<c:url value="/j_spring_security_logout" />" class="btn btndanger btn-mini pull-right">desconnectar</a>
                            </li>
                        </sec:authorize>
                    </ul>
                </nav>
            </div>
        </div>
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>${banner}</h1>
                    <p>${tagline}</p>
                </div>
            </div>
        </section>
        <section class="container">
            <form:form modelAttribute="newXollo" class="form-horizontal">
                <fieldset>
                    <legend>Afegir vol</legend>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="codi">Codi</label>
                        <div class="col-lg-10">
                            <form:input id="codi" path="codi" type="text" class="form:input-large"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="titol">Titol</label>
                        <div class="col-lg-10">
                            <form:input id="titol" path="titol" type="text" class="form:input-large"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2"
                               for="desc">Descripció</label>
                        <div class="col-lg-10">
                            <form:textarea id="desc" path="desc" rows ="2"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="unitats">Unitats</label>
                        <div class="col-lg-10">
                            <form:input id="unitats" path="numUnitats" type="number" class="form:input-large"/>
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="matricula">Matrícula</label>
                        <div class="col-lg-10">
                            <form:input id="matricula" path="matricula" type="text" class="form:input-large"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="hibrid">Hibrid?</label>
                        <div class="col-lg-10">
                            <form:select id="hibrid" path="hibrid" type="select" class="form:input-large">
                                <form:option value="Si" >Si</form:option>
                                <form:option value="No" >No</form:option>
                            </form:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="marca">Marca</label>
                        <div class="col-lg-10">
                            <form:input id="marca" path="marca" type="text" class="form:input-large"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <input type="submit" id="btnAdd" class="btn btn-primary"
                                   value ="Crear"/>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </section>
    </body>
</html>
