/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.repository.impl;

import cat.xtec.ioc.domain.Hotel;
import cat.xtec.ioc.domain.Lloguer;
import cat.xtec.ioc.domain.Vol;
import cat.xtec.ioc.domain.Xollo;
import cat.xtec.ioc.repository.XolloRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Repository;

/**
 *
 * @author T440S
 */

@Repository
public class InMemoryXolloRepository implements XolloRepository{

    private List<Xollo> llista = new ArrayList<Xollo>();
    
    
    public InMemoryXolloRepository(){
        Vol vol_1 = new Vol("V01", "Titol Vol 1", "Vol número 1", 100, 0, "Miami", new Date(2023-05-30), 0);
        Vol vol_2 = new Vol("V02", "Titol Vol 2", "Vol número 2", 130, 0, "Bucarest", new Date(2023-05-28), 0);
        Vol vol_3 = new Vol("V03", "Titol Vol 3", "Vol número 3", 150, 0, "Miami", new Date(2023-05-20), 0);
        
        Hotel hotel_1 = new Hotel("H01", "Titol Hotel 1", "Hotel número 1", 700, 0, "Miami Beach Resort", "A203", "Mdsffjsa Str 230");
        Hotel hotel_2 = new Hotel("H02", "Titol Hotel 2", "Hotel número 2", 900, 0, "Bucarest Luxury Hotel", "560", "Str Mdsffjsa 230");
        Hotel hotel_3 = new Hotel("H03", "Titol Hotel 3", "Hotel número 3", 450, 0, "Miami Hotel", "300", "Street Mdsffjsa 200");
        
        Lloguer lloguer_1 = new Lloguer("L01", "Titol Lloguer 1", "Lloguer número 1", 1, 0, "X-9989-CD", false, "Mustang");
        Lloguer lloguer_2 = new Lloguer("L02", "Titol Lloguer 2", "Lloguer número 2", 1, 0, "9989-LCD", true, "Toyota");
        Lloguer lloguer_3 = new Lloguer("L03", "Titol Lloguer 3", "Lloguer número 3", 1, 0, "X-9900-CD", false, "GMC");
        
        llista.add(vol_1);
        llista.add(vol_2);
        llista.add(vol_3);
        llista.add(hotel_1);
        llista.add(hotel_2);
        llista.add(hotel_3);
        llista.add(lloguer_1);
        llista.add(lloguer_2);
        llista.add(lloguer_3);
        

    }
    
    public void addXollo(Xollo xollo) {
        llista.add(xollo);
    }

    public Xollo getXolloByCodi(String codi) {
        Xollo oferta = null;
        for(Xollo xollo : llista){
            if(xollo!=null && xollo.getCodi()!=null && xollo.getCodi().equals(codi)){
                oferta = xollo;
                break;
            }
        }
        
        if(oferta == null){
            throw new IllegalArgumentException("No s'ha trobat el xollo amb el codi: "+codi+".");
        }
        
        return oferta;
    }

    /*
    3.  Set<Xollo> getXolloByFilter(Map<String, List<String>> filterParams);
        → Ha de respondre a la consulta de filtres que s'ha definit als casos d'ús.  
        Els criteris són: El tipus de Xollo (Vol, Hotel o Lloguer) i el títol del Xollo.



        → Per realitzar aquest mètode podeu ajudar-vos de la funció:
        Class.forName(“nomPaquet.nomClasse).isInstance(Object)
        Aquesta funció comproba que l’objecte Object sigui de la classe “nomPaquet.nomClasse”

    */
    public Set<Xollo> getXolloByFilter(Map<String, List<String>> filterParams) {
        
        Set<Xollo> tipusXollo = new HashSet<Xollo>();
        Set<Xollo> titolXollo = new HashSet<Xollo>();
        
        Set<String> criterias = filterParams.keySet();
        
        if(criterias.contains("tipus")){
            
            for(String tipus : filterParams.get("tipus")){
                for(Xollo xollo : llista){
                    try {
                        if(Class.forName("cat.xtec.ioc.domain.Vol").isInstance(xollo)&&tipus.equals("Vol")){
                            tipusXollo.add(xollo);
                        }else if(Class.forName("cat.xtec.ioc.domain.Hotel").isInstance(xollo)&&tipus.equals("Hotel")){
                            tipusXollo.add(xollo);
                        }else if(Class.forName("cat.xtec.ioc.domain.Lloguer").isInstance(xollo)&&tipus.equals("Lloguer")){
                            tipusXollo.add(xollo);
                        }
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(InMemoryXolloRepository.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        }
        
        if(criterias.contains("title")){
            for(String title : filterParams.get("title")){
                for(Xollo xollo : llista){
                    if(xollo.getTitol().contains(title)){
                        titolXollo.add(xollo);
                    }
                }
            }
        }
        
        tipusXollo.retainAll(titolXollo);
        
        return tipusXollo;
        
        
    }
    
}
