/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cat.xtec.ioc.service;

import cat.xtec.ioc.domain.Xollo;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author T440S
 */
public interface XolloService {
    void addXollo(Xollo xollo);
    Xollo getXolloByCodi(String codi);
    Set<Xollo> getXolloByFilter(Map<String, List<String>> filterParams);
    void processVenda(String codiXollo);
}
