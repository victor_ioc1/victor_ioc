/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.service.impl;

import cat.xtec.ioc.domain.Xollo;
import cat.xtec.ioc.repository.XolloRepository;
import cat.xtec.ioc.service.XolloService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author T440S
 */

@Service
public class XolloServiceImpl implements XolloService{
    
    @Autowired
    private XolloRepository xolloRepository;

    public void addXollo(Xollo xollo) {
        xolloRepository.addXollo(xollo);
    }

    public Xollo getXolloByCodi(String codi) {
        return xolloRepository.getXolloByCodi(codi);
    }

    public Set<Xollo> getXolloByFilter(Map<String, List<String>> filterParams) {
        return xolloRepository.getXolloByFilter(filterParams);
    }

    public void processVenda(String codiXollo) {
        Xollo xollo;
        if(xolloRepository.getXolloByCodi(codiXollo)!=null){
            xollo = xolloRepository.getXolloByCodi(codiXollo);
            
            if(xollo.getNumUnitats()>0){
                xollo.setNumReserves(xollo.getNumReserves()+1);
                
                xollo.setNumUnitats(xollo.getNumUnitats()-1);
            }else{
                throw new IllegalArgumentException("No hi ha suficients xollos amb el codi: "+codiXollo+".");
            }
        }
        
    }
    
}
