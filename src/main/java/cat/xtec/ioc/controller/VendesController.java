/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.controller;

import cat.xtec.ioc.service.XolloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author T440S
 */

@Controller
public class VendesController {
    
    @Autowired
    private XolloService xolloService;
    
    
    @RequestMapping(value="/vendaXollo/{codi}", method=RequestMethod.GET)
    public String vendre(@PathVariable("codi") String nomVar){
        xolloService.processVenda(nomVar);
        
        return "redirect:/getXollo/"+nomVar;
    }
}
