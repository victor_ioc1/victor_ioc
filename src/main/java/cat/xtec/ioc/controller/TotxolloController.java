/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.controller;

import cat.xtec.ioc.domain.Hotel;
import cat.xtec.ioc.domain.Lloguer;
import cat.xtec.ioc.domain.Vol;
import cat.xtec.ioc.domain.Xollo;
import cat.xtec.ioc.service.XolloService;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author T440S
 */
@Controller
@RequestMapping("/")
public class TotxolloController {
    
    @Autowired
    private XolloService xolloService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("home");
        
        //Llista on es guarda la informació de les diferents funcionalitats
        List<Map<String, String>> informacio = new ArrayList<Map<String, String>>();
        
        //Opció Afegir
        Map<String, String> afegir = new HashMap<String, String>();
        afegir.put("title", "Afegir");
        afegir.put("desc", "Permet afegir un article al catàleg de la botiga");
        afegir.put("url", "/add");
        afegir.put("icon", "glyphicon-plus-sign glyphicon");
        informacio.add(afegir);
        
        //Opció Consulta
        Map<String, String> consulta = new HashMap<String, String>();
        consulta.put("title", "Consultar");
        consulta.put("desc", "Permet consultar la informació d'un article al catàleg de la botiga");
        consulta.put("url", "/get");
        consulta.put("icon", "glyphicon glyphicon-search");
        informacio.add(consulta);
        
        //Opció Filtrar
        Map<String, String> filtrar = new HashMap<String, String>();
        filtrar.put("title", "Filtrar");
        filtrar.put("desc", "Permet cercar, dins el catàleg de la botiga, algun article");
        filtrar.put("url", "/filter");
        filtrar.put("icon", "glyphicon glyphicon-filter");
        informacio.add(filtrar);
        
        //Opció Comprar
        Map<String, String> comprar = new HashMap<String, String>();
        comprar.put("title", "Comprar");
        comprar.put("desc", "Permet comprar un article de la botiga");
        comprar.put("url", "/venda");
        comprar.put("icon", "glyphicon glyphicon-shopping-cart");
        informacio.add(comprar);
        
        String TituloBanner = "TINC POR PER L'EXAMEN, perque encara que he aprés molt fent el mòdul 8 de DAW ha estat MOOOLT dur!!!!";
        
        modelview.getModelMap().addAttribute("banner", TituloBanner);
        modelview.getModelMap().addAttribute("options", informacio);
        
        return modelview;
    }
    
    @RequestMapping(value="/add", method = RequestMethod.GET)
    public ModelAndView addOptions(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("afegir");
        
        //Llista on es guarda la informació de les diferents funcionalitats
        List<Map<String, String>> informacio = new ArrayList<Map<String, String>>();
        
        //Opció Vol
        Map<String, String> vol = new HashMap<String, String>();
        vol.put("title", "Vol");
        vol.put("desc", "Permet afegir un vol al catàleg de la botiga");
        vol.put("url", "/add/Vol");
        vol.put("icon", "glyphicon glyphicon-plane");
        informacio.add(vol);
        
        //Opció Hotel
        Map<String, String> hotel = new HashMap<String, String>();
        hotel.put("title", "Hotel");
        hotel.put("desc", "Permet afegir un hotel al catàleg de la botiga");
        hotel.put("url", "/add/Hotel");
        hotel.put("icon", "glyphicon glyphicon-bed");
        informacio.add(hotel);
        
        //Opció Lloguer
        Map<String, String> lloguer = new HashMap<String, String>();
        lloguer.put("title", "Lloguer");
        lloguer.put("desc", "Permet afegir un model de cotxe de lloguer al catàleg de la botiga");
        lloguer.put("url", "/add/Lloguer");
        lloguer.put("icon", "glyphicon glyphicon-road");
        informacio.add(lloguer);
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Afegir un article al catàleg");
        modelview.getModelMap().addAttribute("options", informacio);
        
        return modelview;
    }
    
    @RequestMapping(value="/add/{tipus}", method=RequestMethod.GET)
    public ModelAndView addXolloForm(@PathVariable("tipus") String tipus, HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException{
        ModelAndView modelview = new ModelAndView("add"+tipus);
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Afegir un article de tipus " + tipus + " al catàleg");
        
        modelview.getModelMap().addAttribute("newXollo", Class.forName("cat.xtec.ioc.domain."+tipus).newInstance());
        
        
        return modelview;
    } 
    
    @InitBinder
    public void initialiseBinder(WebDataBinder binder){
        binder.setDisallowedFields("numReserves");
    }
    
    @RequestMapping(value="/add/{tipus}", method=RequestMethod.POST)
    public String addXolloForm(@PathVariable("tipus") String tipus, @ModelAttribute("newXollo") Xollo newXollo, BindingResult result) throws IllegalArgumentException, IllegalAccessException{
        String[] supressedFields = result.getSuppressedFields();
        if(supressedFields.length>0){
            throw new RuntimeException("Attempting to bind disallowed fields: " + StringUtils.arrayToCommaDelimitedString(supressedFields));
            
        }
        /* 
        ESTE EJERCICIO NO ME SALE EN ABSOLUTO. NO SÉ COMO PASAR DE UN XOLLO A UN VUELO 
        AÑADIENDOLE LA INFORMACIÓN AL OBJETO VUELO. INFORMACIÓN QUE CONTIENE newXollo. 
        Que no esntiendo como puede ser que un objeto Xollo contenga la información completa 
        del formulario
        
        
        if(tipus.equals("Vol")){
            
            newXollo = new Vol();
            
           
            
        }else if(tipus.equals("Hotel")){
            newXollo = new Hotel();
        }else{
            newXollo = new Lloguer();
        }
*/
        xolloService.addXollo(newXollo);
        
        return "redirect:/";
        
    }
    
    
    
    @RequestMapping(value="/get", method = RequestMethod.GET)
    public ModelAndView getXolloFormRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("getXolloForm");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        
        return modelview;
    }
    
    @RequestMapping(value="/get", method=RequestMethod.POST)
    public ModelAndView getXolloByCodiRequest(@RequestParam String codi, HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("infoXollo");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Dades d'un xollo");
        
        modelview.getModelMap().addAttribute("xollo", xolloService.getXolloByCodi(codi));
        
        return modelview;
        
    }
    
    @RequestMapping(value="/getXollo/{codi}", method=RequestMethod.GET)
    public ModelAndView getXolloByCodiVendaRequest(@PathVariable("codi") String codi, HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("infoXollo");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Dades d'un article");
        
        modelview.getModelMap().addAttribute("xollo", xolloService.getXolloByCodi(codi));
        
        return modelview;
        
    }
    
    @RequestMapping(value="/filter", method=RequestMethod.GET)
    public ModelAndView getXolloByFilter(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("helpFilter");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Ajuda per la creació d'un Filtre");
        modelview.getModelMap().addAttribute("title", "Creació d'un filtre del tipus: ByCriteria");
        
        List<String> comentaris = new ArrayList<String>();
        
        String coment_1 = "Heu de canviar la URL per aquest patró:";
        String coment_2 = "tipus=Vol,Hotel;title=titol,3";
        String coment_3 = "On Tipus por ser els noms de les classes de domini (exactament igual): Vol, Hotel o Lloguer";
        String coment_4 = "Per exemple si el tipus és Vol i Hotel i el title és titol i 3 la URL seria la que conté aquest botó: ";
        
        comentaris.add(coment_1);
        comentaris.add(coment_2);
        comentaris.add(coment_3);
        comentaris.add(coment_4);
        
        modelview.getModelMap().addAttribute("comments", comentaris);
        
        return modelview;
    } 
    
    @RequestMapping(value="/filter/{ByCriteria}", method=RequestMethod.GET)
    public ModelAndView getXolloByFilter(@MatrixVariable(pathVar="ByCriteria") Map<String, List<String>> filterParam, HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("listXolloByFilter");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Llistat d'articles que compleixen els requisits.");
        modelview.getModelMap().addAttribute("xollo", xolloService.getXolloByFilter(filterParam));
        
        return modelview;
    } 
    
    @RequestMapping(value="/venda", method=RequestMethod.GET)
    public ModelAndView getVenda(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        ModelAndView modelview = new ModelAndView("helpVendes");
        
        modelview.getModelMap().addAttribute("banner", "Busca un xollo!");
        modelview.getModelMap().addAttribute("tagline", "Ajuda per la venda d'un article.");
        modelview.getModelMap().addAttribute("title", "Venda");
        
        List<String> comentaris = new ArrayList<String>();
        
        String coment_1 = "Heu de canviar la URL per aquest patró:";
        String coment_2 = "/vendaXollo/codi";
        String coment_3 = "Per exemple si voleu fer una venda pel vol 1 seria /vendaXollo/V01 ";
        
        comentaris.add(coment_1);
        comentaris.add(coment_2);
        comentaris.add(coment_3);
        
        modelview.getModelMap().addAttribute("comments", comentaris);
        
        return modelview;
    } 

}
