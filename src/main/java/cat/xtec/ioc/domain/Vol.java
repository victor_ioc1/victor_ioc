/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain;

import java.util.Date;

/**
 *
 * @author T440S
 */
public class Vol extends Xollo{
    private String desti;
    private Date data;
    private int persones;
    
    
    //Contructors
    public Vol(){
        super();
    }
    
    public Vol(String codi, String titol, String desc, int unitats, int reserves, String desti, Date data, int persones) {
        super(codi, titol, desc, unitats, reserves);
        this.desti = desti;
        this.data = data;
        this.persones = persones;
    }
    
    //Getters
    public String getDesti(){
        return this.desti;
    }
    public Date getData(){
        return this.data;
    }
    public int getPersones(){
        return this.persones;
    }
    
    //Setters
    public void setDesti(String desti){
        this.desti = desti;
    }
    public void setData(Date data){
        this.data = data;
    }
    public void setPersones(int persones){
        this.persones = persones;
    }
}
