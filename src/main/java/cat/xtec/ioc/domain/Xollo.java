/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain;

/**
 *
 * @author T440S
 */
public class Xollo {
    private String codi;
    private String titol;
    private String desc;
    private int numUnitats;
    private int numReserves;
    
    //Constructors
    public Xollo(){}
    
    public Xollo(String codi, String titol){
        this.codi = codi;
        this.titol = titol;
        this.desc = "";
        this.numUnitats = 0;
        this.numReserves = 0;
    }
    public Xollo(String codi, String titol, String desc){
        this.codi = codi;
        this.titol = titol;
        this.desc = desc;
        this.numUnitats = 0;
        this.numReserves = 0;
    }
    public Xollo(String codi, String titol, String desc, int unitats, int reserves){
        this.codi = codi;
        this.titol = titol;
        this.desc = desc;
        this.numUnitats = unitats;
        this.numReserves = reserves;
    }
    
    //Getters
    public String getCodi(){
        return this.codi;
    }
    public String getTitol(){
        return this.titol;
    }
    public String getDesc(){
        return this.desc;
    }
    public int getNumUnitats(){
        return this.numUnitats;
    }
    public int getNumReserves(){
        return this.numReserves;
    }
    
    //Setters
    public void setCodi(String codi){
        this.codi = codi;
    }
    public void setTitol(String titol){
        this.titol = titol;
    }
    public void setDesc(String desc){
        this.desc = desc;
    }
    public void setNumUnitats(int numUnitats){
        this.numUnitats = numUnitats;
    }
    public void setNumReserves(int numReserves){
        this.numReserves = numReserves;
    }
}
