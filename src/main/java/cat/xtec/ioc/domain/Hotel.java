/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain;

/**
 *
 * @author T440S
 */
public class Hotel extends Xollo{
    private String nom;
    private String habitacio;
    private String direccio;

    
    //Constructors
    public Hotel(){
        super();
    }
    
    public Hotel(String codi, String titol, String desc, int unitats, int reserves, String nom, String room, String address){
        super(codi, titol, desc, unitats, reserves);
        this.nom = nom;
        this.habitacio = room;
        this.direccio = address;
    }
    
    
    //Getters
    public String getNom(){
        return this.nom;
    }
    public String getHabitacio(){
        return this.habitacio;
    }
    public String getDireccio(){
        return this.direccio;
    }
    
    //Setters
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setHabitacio(String habitacio){
        this.habitacio = habitacio;
    }
    public void setDireccio(String address){
        this.direccio = address;
    }
    
}
