/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.xtec.ioc.domain;

/**
 *
 * @author T440S
 */
public class Lloguer extends Xollo{
    private String matricula;
    private Boolean hibrid;
    private String marca;
    
    //Constructors
    public Lloguer(){
        super();
    }
    
    public Lloguer(String codi, String titol, String desc, int unitats, int reserves, String matricula, Boolean hibrid, String marca) {
        super(codi, titol, desc, unitats, reserves);
        
        this.matricula = matricula;
        this.hibrid = hibrid;
        this.marca = marca;
    }
    
    //Getters
    public String getMatricula(){
        return this.matricula;
    }
    public Boolean getHibrid(){
        return this.hibrid;
    }
    public String getMarca(){
        return this.marca;
    }
    
    //Setters
    public void setMatricula(String matricula){
        this.matricula = matricula;
    }
    public void setHibrid(Boolean hibrid){
        this.hibrid = hibrid;
    }
    public void setMarca(String marca){
        this.marca = marca;
    }
    
}
